
from django.contrib import admin
from django.urls import path
from authApp import views  as authAppViews
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('game/create/',authAppViews.GameCreateView.as_view()), # crear un juego
    path('game/<int:pk>/',authAppViews.GameDetailView.as_view()), # mostrar un juego
    path('user/create/',authAppViews.UserCreateView.as_view()), # crear un usuario
    path('user/<int:pk>/',authAppViews.UserDetailView.as_view()), # modificar, eliminar un usuario
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
]
