from django.conf                               import settings
from rest_framework                            import generics, status
from rest_framework.response                   import Response
from rest_framework.permissions                import IsAuthenticated
from rest_framework_simplejwt.backends         import TokenBackend

from authApp.models.game                      import Game
from authApp.serializers.gameSerializer        import GameSerializer
from django.http import Http404

class GameDetailView(generics.RetrieveAPIView):
    queryset           = Game.objects.all()
    serializer_class   = GameSerializer
    
    def get(self, request, *args, **kwargs): 
        print(args)    
        return super().get(request, *args, **kwargs)
    
             
    """ def delete(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        game = Game.objects.get(pk=pk)
        game.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, *args, **kwargs):
        pk = kwargs["pk"]
        game = Game.objects.get(pk=pk)
        serializer = GameSerializer(game, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) """