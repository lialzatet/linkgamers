from django.db import models
from .user import User

class Amistad(models.Model):
    id = models.AutoField(primary_key=True)
    user1 = models.ForeignKey('User', related_name='UsuarioUno', on_delete=models.CASCADE)
    user2 = models.ForeignKey('User', related_name='UsuarioDos', on_delete=models.CASCADE)