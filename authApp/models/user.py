from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):

  def create_user(self, username, password=None):
#"Creates and saves a user with the given username and password."
     if not username:
       raise ValueError('Users must have an username')
     user = self.model(username=username)
     user.set_password(password)
     user.save(using=self._db)
     return user
    
  def create_superuser(self, username, password=None):
#"Creates and saves a superuser with the given username and password."
      user = self.create_user(
      username=username,
      password=password,
        )
      user.is_admin = True
      user.is_superuser = True
      user.save(using=self._db)
      return user 

class User(AbstractBaseUser, PermissionsMixin):


     id = models.BigAutoField(primary_key=True)
     username = models.CharField('Username', max_length=15, unique=True)
     password = models.CharField('Password', max_length=256)     
     email = models.EmailField('Email', max_length=100)
     friends = models.ManyToManyField("self", through='Amistad',through_fields=('user1','user2') )
     games = models.ManyToManyField('Game', through='user_game', through_fields=('user', 'game'), related_name='players')
     linkFoto = models.CharField('LinkFoto', max_length=300, default="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png")
     presentacion= models.CharField('Presentacion', max_length=200, default="Hola, soy un nuevo jugador con ganas de conocer compañeros de juegos")

     def save(self, **kwargs):
         some_salt = 'mMUj0DrIK6vgtdIYepkIxN'
         self.password = make_password(self.password, some_salt)
         super().save(**kwargs)
         
     objects = UserManager()

     USERNAME_FIELD = 'username'
     REQUIRED_FIELDS = []
