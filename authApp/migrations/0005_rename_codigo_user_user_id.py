# Generated by Django 3.2.8 on 2021-10-19 22:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authApp', '0004_auto_20211019_1656'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='codigo_user',
            new_name='id',
        ),
    ]
